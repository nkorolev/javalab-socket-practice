package by.gsu.po42.nkorolev.client;

import by.gsu.po42.nkorolev.protocol.ifaces.Message;
import by.gsu.po42.nkorolev.protocol.ifaces.MessageType;
import by.gsu.po42.nkorolev.protocol.ifaces.Transport;
import by.gsu.po42.nkorolev.protocol.impl.BinaryMessage;
import by.gsu.po42.nkorolev.protocol.impl.BinaryTransport;
import by.gsu.po42.nkorolev.protocol.impl.TransferException;

import java.io.IOException;
import java.net.Socket;
import java.util.List;

public class Client implements AutoCloseable {
    private Transport transfer;
    private Socket client;

    public Client(String host, int port) throws TransferException {
        try {
            client = new Socket(host, port);
            transfer = new BinaryTransport(client);
        } catch (IOException e) {
            throw new TransferException("Error while connecting.", e);
        }
    }

    public Client create(Object object) throws TransferException {
        transfer.sendMessage(new BinaryMessage().setType(MessageType.CREATE).setAttribute("obj", object));
        return this;
    }

    public List<Object> read() throws TransferException {
        transfer.sendMessage(new BinaryMessage().setType(MessageType.READ).setAttribute("b", 11));
        return (List<Object>) transfer.readMessage().getAttribute("obj");
    }

    public Client update(int id, Object replacement) throws TransferException {
        Message msg = new BinaryMessage()
                .setType(MessageType.UPDATE)
                .setAttribute("id", id)
                .setAttribute("obj", replacement);
        transfer.sendMessage(msg);
        return this;
    }

    public Client delete(int id) throws TransferException {
        transfer.sendMessage(new BinaryMessage().setType(MessageType.DELETE).setAttribute("id", id));
        return this;
    }

    @Override
    public void close() throws TransferException {
        //TODO: make safe closing
        transfer.sendMessage(new BinaryMessage().setType(MessageType.EXIT));
        try {
            transfer.close();
            client.close();
        } catch (IOException e) {
            throw new TransferException("Error while closing.", e);
        }
    }
}
