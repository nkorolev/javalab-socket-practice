package by.gsu.po42.nkorolev.client;

import by.gsu.po42.nkorolev.bean.Item;
import by.gsu.po42.nkorolev.protocol.ifaces.MessageType;
import by.gsu.po42.nkorolev.protocol.impl.TransferException;
import by.gsu.po42.nkorolev.utils.Console;

import java.io.IOException;
import java.util.Scanner;

public class ClientRunner {
    public static Item readItem(Scanner scanner) {
        Item result = new Item();
        System.out.println("Enter material:");
        result.setMaterial(scanner.next());
        System.out.println("Enter name:");
        result.setName(scanner.next());
        System.out.println("Enter height:");
        result.setHeight(scanner.nextDouble());
        System.out.println("Enter width:");
        result.setWidth(scanner.nextDouble());
        System.out.println("Enter length:");
        result.setLength(scanner.nextDouble());
        return result;
    }

    public static int readId(Scanner scanner) {
        System.out.println("Enter id:");
        return scanner.nextInt();
    }

    public static void welcome() {
        System.out.println("Hello. This is client console.");
        System.out.print("Available commands: ");
        MessageType[] messages = MessageType.values();
        for (int i = 0; i < messages.length; i++) {
            System.out.print(messages[i].toString().toLowerCase());
            if (i == messages.length - 1) {
                System.out.print(".");
            } else {
                System.out.print(", ");
            }
        }
        System.out.println();
        System.out.println("Type one of command below to start.");
    }

    public static void main(String[] args) {
        try (Client client = new Client("localhost", 1337);
             Scanner scanner = new Scanner(System.in)) {

            while (true) {
                welcome();
                MessageType type = MessageType.valueOf(scanner.next().toUpperCase());

                switch (type) {
                    case CREATE:
                        client.create(readItem(scanner));
                        break;
                    case READ:
                        System.out.println(client.read());
                        break;
                    case UPDATE:
                        client.update(readId(scanner), readItem(scanner));
                        break;
                    case DELETE:
                        client.delete(readId(scanner));
                        break;
                    case EXIT:
                        Console.log("Bye, bye.");
                        return;
                }

            }
        } catch (TransferException e) {
            e.printStackTrace();
        }
    }
}
