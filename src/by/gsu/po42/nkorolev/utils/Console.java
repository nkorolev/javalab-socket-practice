package by.gsu.po42.nkorolev.utils;


import java.text.SimpleDateFormat;
import java.util.Date;

public class Console {
    public static void log(String msg, Object... args) {
        log(msg);
        for (int i = 0; i < args.length; i++) {
            log("Data " + i + ": " + args[i]);
        }
    }

    public static void log(String msg) {
        String timestamp = new SimpleDateFormat("hh:mm:ss dd-MM-yyyy").format(new Date());
        System.out.printf("[%s] %s%n", timestamp, msg);
    }
}
