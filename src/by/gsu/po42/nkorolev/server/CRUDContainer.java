package by.gsu.po42.nkorolev.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

//TODO: change thread safe container by thread safe methods, to speed up performance
public class CRUDContainer {
    private List<Object> container = new Vector<>();

    public CRUDContainer create(Object obj) {
        container.add(obj);
        return this;
    }

    public List<Object> read() {
        return new ArrayList<>(container);

    }

    public CRUDContainer update(int id, Object obj) {
        //TODO: optimize "double locking"
        synchronized (container) {
            container.remove(id);
            container.add(id, obj);
        }
        return this;
    }

    public CRUDContainer delete(int id) {
        container.remove(id);
        return this;
    }
}
