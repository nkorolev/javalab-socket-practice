package by.gsu.po42.nkorolev.server;

import by.gsu.po42.nkorolev.utils.Console;

import java.io.IOException;
import java.net.ServerSocket;

public class Server implements AutoCloseable {
    private CRUDContainer list = new CRUDContainer();
    private ServerSocket server;

    public Server(int port) throws IOException {
        server = new ServerSocket(port);
        Console.log("Server started");
        while (true) {
            new ConnectionHandler(list, server.accept()).start();
        }
    }

    @Override
    public void close() throws IOException {
        Console.log("Server shutdown");
        server.close();
    }
}
