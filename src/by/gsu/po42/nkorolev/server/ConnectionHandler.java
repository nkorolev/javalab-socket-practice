package by.gsu.po42.nkorolev.server;


import by.gsu.po42.nkorolev.protocol.ifaces.Message;
import by.gsu.po42.nkorolev.protocol.ifaces.Transport;
import by.gsu.po42.nkorolev.protocol.impl.BinaryMessage;
import by.gsu.po42.nkorolev.protocol.impl.BinaryTransport;
import by.gsu.po42.nkorolev.utils.Console;

import java.io.IOException;
import java.net.Socket;

public class ConnectionHandler extends Thread {
    private Transport transfer;
    private CRUDContainer list;
    private Socket connection;

    public ConnectionHandler(CRUDContainer list, Socket connection) throws IOException {
        this.list = list;
        this.transfer = new BinaryTransport(connection);
        this.connection = connection;
    }

    @Override
    public void run() {
        Console.log("Client connected");
        try {
            while (true) {
                Message msg = transfer.readMessage();
                Console.log("Message received.", msg);
                switch (msg.getType()) {
                    case CREATE:
                        list.create(msg.getAttribute("obj"));
                        break;
                    case READ:
                        transfer.sendMessage(new BinaryMessage().setAttribute("obj", list.read()));
                        break;
                    case UPDATE:
                        list.update((int) msg.getAttribute("id"), msg.getAttribute("obj"));
                        break;
                    case DELETE:
                        list.delete((int) msg.getAttribute("id"));
                        break;
                    case EXIT:
                        Console.log("Client disconnected.");
                        transfer.close();
                        connection.close();
                        return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
