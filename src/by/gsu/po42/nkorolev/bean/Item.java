package by.gsu.po42.nkorolev.bean;

import java.io.Serializable;

public class Item implements Serializable {
    private String material;
    private String name;
    private double width;
    private double height;
    private double length;

    public Item() {
    }

    public Item(String material, String name, float width, float height, float length) {
        this.material = material;
        this.name = name;
        this.width = width;
        this.height = height;
        this.length = length;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "Item{" +
                "material='" + material + '\'' +
                ", name='" + name + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", length=" + length +
                '}';
    }
}
