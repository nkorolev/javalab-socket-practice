package by.gsu.po42.nkorolev.protocol.ifaces;

import java.util.Map;

public interface Message {
    Map<String, Object> getAttributes();

    Message setAttributes(Map<String, Object> attributes);

    Object getAttribute(String key);

    Message setAttribute(String key, Object value);

    MessageType getType();

    Message setType(MessageType type);
}
