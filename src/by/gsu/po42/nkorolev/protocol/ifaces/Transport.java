package by.gsu.po42.nkorolev.protocol.ifaces;

import by.gsu.po42.nkorolev.protocol.impl.TransferException;

import java.io.Closeable;

public interface Transport extends Closeable {
    Transport sendMessage(Message msg) throws TransferException;

    Message readMessage() throws TransferException;
}
