package by.gsu.po42.nkorolev.protocol.ifaces;

public enum MessageType {
    CREATE,
    READ,
    UPDATE,
    DELETE,
    EXIT;
}
