package by.gsu.po42.nkorolev.protocol.impl;

import by.gsu.po42.nkorolev.protocol.ifaces.Message;

import java.io.IOException;

public class TransferException extends IOException {
    private Message msg;

    public TransferException(String message, Throwable cause, Message msg) {
        super(message, cause);
        this.msg = msg;
    }

    public TransferException(String message, Throwable cause) {
        super(message, cause);
    }
}
