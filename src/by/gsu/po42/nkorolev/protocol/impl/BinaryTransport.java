package by.gsu.po42.nkorolev.protocol.impl;

import by.gsu.po42.nkorolev.protocol.ifaces.Message;
import by.gsu.po42.nkorolev.protocol.ifaces.Transport;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class BinaryTransport implements Transport {
    private ObjectOutputStream oos;
    private ObjectInputStream ois;

    public BinaryTransport(Socket target) throws TransferException {
        try {
            oos = new ObjectOutputStream(target.getOutputStream());
            ois = new ObjectInputStream(target.getInputStream());
        } catch (IOException e) {
            throw new TransferException("Error on stream opening.", e);
        }
    }

    @Override
    public Transport sendMessage(Message msg) throws TransferException {
        try {
            oos.writeObject(msg);
            return this;
        } catch (IOException e) {
            throw new TransferException("Error while sending message", e, msg);
        }
    }

    @Override
    public Message readMessage() throws TransferException {
        try {
            return (Message) ois.readObject();
        } catch (ClassNotFoundException | IOException e) {
            throw new TransferException("Error while message receive.", e);
        }
    }

    @Override
    public void close() throws TransferException {
        //TODO: make safe closing
        try {
            oos.close();
            ois.close();
        } catch (IOException e) {
            throw new TransferException("Error while closing.", e);
        }
    }
}
