package by.gsu.po42.nkorolev.protocol.impl;

import by.gsu.po42.nkorolev.protocol.ifaces.Message;
import by.gsu.po42.nkorolev.protocol.ifaces.MessageType;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class BinaryMessage implements Message, Serializable {
    private Map<String, Object> attributes = new HashMap<>();
    private MessageType type;

    @Override
    public Map<String, Object> getAttributes() {
        return attributes;
    }

    @Override
    public Message setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
        return this;
    }

    @Override
    public Object getAttribute(String key) {
        return attributes.get(key);
    }

    @Override
    public Message setAttribute(String key, Object value) {
        attributes.put(key, value);
        return this;
    }

    @Override
    public MessageType getType() {
        return type;
    }

    @Override
    public Message setType(MessageType type) {
        this.type = type;
        return this;
    }

    @Override
    public String toString() {
        return "BinaryMessage{" +
                "attributes=" + attributes +
                ", type=" + type +
                '}';
    }
}
